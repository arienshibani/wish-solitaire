# Wish Solitaire #

A simple command line interface game of wish-solitaire. 

Discard cards that have the same value until there are none left!

[![GAMEPLAY-GIF](http://g.recordit.co/OU4ADnAWMJ.gif)]()



### System Requirements ###
* Python 3.6 or newer
* At least 1MB free disk space

## Installing ##
1. Clone or [download](http://semver.org/)  this repository to desktop.
2. Run the Python file by writing`python3 wishsolitaire.py` in the command line interface.
3. Enjoy the game. Official gameplay rules found [here](https://bicyclecards.com/how-to-play/wish-solitaire/)
