# ---------------------------------------------------------------------------
#
#  A simple command line interface game of wish-solitaire, written in python.
#
#      + Official Rules for the game:
#           https://bicyclecards.com/how-to-play/wish-solitaire/
#
#      + Git Repository:
#           https://bicyclecards.com/how-to-play/wish-solitaire/
#
# ---------------------------------------------------------------------------

import random  # Simulates deck shuffling with randomized decks.
import re  # Regular Expressions Used to validate user keyboard input.


# User Settings
Language = 'NO'
Hearts = '\u2660'
Diamond = '\u2666'
Cloves = '\u2663'
Spades = '\u2665'


class Card:
    '''
    An object that represents a playing card. It has a suite, and a value.

     Parameters:
        value (int): the cards value, defaults to 1.
        suit (string): the cards suit, default to Spades escape symbol.

    '''

    def __init__(self, value: int = 1, suit: str = Spades):
        '''Values 1, 11, 12 and 13 will be cast to strings: 'A', 'J', 'Q' and 'K' respectively.'''

        if value == 11:
            value = 'J'
        elif value == 12:
            value = 'Q'
        elif value == 13:
            value = 'K'
        elif value == 1:
            value = 'A'

        self.suit = suit
        self.value = value

    def print(self) -> str:
        '''Visual representation of this specific card in-game.'''
        return '|' + self.suit + str(self.value) + '|'


class Deck:
    '''
    The deck object is responsible for representing piles of cards (card objects), of varying size.
    Furthermore it contains the methods needed to set up a game of wish solitaire.

    Parameters:
        list_of_cards (List): A list that contains all cards, defualts to empty list.
        ID (str): A letter that identifies the deck object, defaults to 'A'.

    '''

    def __init__(self, list_of_cards: list = [], ID: str = 'A'):
        self.cards = list_of_cards
        self.ID = ID

    def append_cards_to_deck(self, wish=True):
        '''
        Fills up this deck object's list with playing cards.

        Parameters:
            wish (boolean): builds a wish solitaire deck if true, defaults to True.
            otherwise, it builds a standard 52 card deck.
        '''
        for i in range(1, 14):
            if wish == True and i > 1 and i < 7:
                continue
            self.cards.append(Card(i, Hearts))
            self.cards.append(Card(i, Diamond))
            self.cards.append(Card(i, Cloves))
            self.cards.append(Card(i, Spades))

    def shuffle_deck(self):
        '''Shuffles list of cards in a deck.'''
        random.shuffle(self.cards)

    def print_top_card(self):
        '''Prints the top card of a deck.'''
        try:
            return self.cards[0].print()
        except:
            return '|-X|'

    def get_top_card(self):
        '''Returns the top card object.'''
        if len(self.cards) != 0:
            return self.cards[0]


class Game:
    '''The game object's responsibility is to enforce the game logic and rules. It facilitates the game play.'''

    def __init__(self):
        self.number_of_turns = 0
        self.card_piles = []  # A list of each card pile in the game.
        self.game_over = False

        new_deck = Deck()
        new_deck.append_cards_to_deck()
        new_deck.shuffle_deck()

        # Construct 8 seperate card pile ID's using list comprehensions...
        card_pile_ids = [chr(i) for i in range(ord('A'), ord('H')+1)]
        self.card_piles = [get_sub_deck(new_deck, i*4, i*4+4, pile_id)  # ...and distributes the 32 cards across them.
                           for i, pile_id in enumerate(card_pile_ids)]

    def save_game(self):
        '''Writes the current game state (every card in each pile) to a save file '''
        with open('game_save.txt', 'w', encoding='UTF-8') as save_file:
            for deck in self.card_piles:
                for card in deck.cards:
                    save_file.write(card.suit + str(card.value) + ' ')
                save_file.write('\n')

    def load_game(self):
        '''Reads a save file, if it exists, and instantiates that game.'''
        self.card_piles = []  # Reset the card piles in this game object.
        with open('game_save.txt', 'r', encoding='UTF-8') as save_file:
            # By enumerating we get the deck ID via index i.
            for i, line in enumerate(save_file):
                # Will be sent to the Deck class constructor.
                card_objects = []
                deck_ID = chr(ord('@')+i+1)
                # Split up the 'save_file' into seperate piles
                saved_card_piles = line.split(' ')
                for saved_card in saved_card_piles:
                    if saved_card != '\n':
                        # Create new card objects and append them to card_objects list.
                        card_objects.append(
                            Card(saved_card[1:], saved_card[0]))
                # Create new deck objects and append them to the game object.
                self.card_piles.append(Deck(card_objects, deck_ID))
        print(f"{' '*10} game_save.txt lastet inn.")

    def render_game(self):
        '''Retrieves all the information about the different piles and their top cards, and print them for the player. '''
        id_strings = [pile.ID for pile in self.card_piles]

        print(f'{"-"*50}')
        print(f'{" "*13} Wish Solitaire - Runde', self.number_of_turns)
        print(f'{"-"*50}')

        # Print out the card pile ID's and their remainaing cards.
        for ID, remaining_cards in zip(id_strings, [len(pile.cards) for pile in self.card_piles]):
            print(f'  {ID}', f'-{remaining_cards}', end=' ', sep='')
        print('\n')

        # Print out each pile's top card.
        for card in [pile.print_top_card() for pile in self.card_piles]:
            print(' ', card, end=' ', sep='')
        print('\n')
        print(f'{"-"*50}')

    def discard_pair(self, pile_A, pile_B):
        '''Takes in two piles, and discards the top cards if they have the same value.'''
        if(pile_A.get_top_card().value == pile_B.get_top_card().value):
            pile_A.cards.pop(0)
            pile_B.cards.pop(0)
        else:
            print(f'{"-"*50}')
            print(f'{" "*5}Ugyldig valg. Kortene må ha samme verdi.')

    def check_game_state(self):
        '''This function checks wether or not the player has won or lost the game.'''

        # Creates a list of all the top cards in all the piles.
        top_cards = [pile.get_top_card().value for pile in self.card_piles if hasattr(
            pile.get_top_card(), 'value')]

        # If all piles of cards are empty, that means the player has won:
        if len(top_cards) == 0:
            self.render_game()
            print("Gratulerer, du vant spillet!")
            cont = input("Tast enter for å gå til hovedmeny >")
            self.game_over = True

        # If the remaining top-cards don't have any duplicates, the player looses the game:
        elif len(top_cards) == len(set(top_cards)):
            self.render_game()
            print("Ingen mulige kombinasjoner, du har tapt spillet!")
            cont = input("Tast enter for å gå til hovedmeny >")
            self.game_over = True

    def start_new_round(self):
        '''
        Initiates a new round of wish solitaire. It begins by checking the game state.
        if the player has not won or lost the game, we display the cardpiles and let the user
        pick two cards to discard.
        '''

        self.check_game_state()

        if self.game_over == False:
            def validate_players_deck_choice(user_input):
                '''
                Reg-Ex used to evaluate the users input.

                    Parameters:
                    - user_input (Str): Input recieved from player when choosing decks

                    Returns:
                    - True / False (bool): True if the user's input string matches the regular expression.

                '''

                regex_expression = r'^([a-hA-H])-(?!\1)[a-hA-H]|0$'

                return bool(re.match(regex_expression, user_input.upper()))

            # This while loop will not break, unless the user saves/quits or enters accepted input.
            while True:
                self.render_game()

                # Retrieve the users input
                user_input = input(
                    'Velg 2 bunker, tast 0 for meny (f.eks: A-B) > ')

                # '0' enables the in-game menu
                if user_input == '0':
                    print(f'{"-"*50}')
                    print(f'{" "*13} 1. Avslutt og gå til hovedmeny')
                    print(f'{" "*13} 2. Lagre spill og gå til hovedmeny')
                    print(f'{" "*13} 3. Regler')
                    print(f'{" "*13} 4. Tilbake til spillet')
                    print(f'{"-"*50}')

                    # Get the users in-game-menu input
                    in_game_menu_choice = input(f'{" " * 10}Valg > ')
                    if in_game_menu_choice == "2":
                        self.save_game()
                        initialize_menu()
                        break
                    if in_game_menu_choice == "1":
                        print(f'{" "*13} Avslutte uten og lagre?')
                        prompt = input(f'{" " * 25} Y/N')
                        if prompt.upper() == "Y":
                            self.save_game()
                            initialize_menu()
                        if prompt.upper() == "N":
                            initialize_menu()
                        break
                    if in_game_menu_choice == "3":
                        print_rules()
                    if in_game_menu_choice == "4":
                        continue

                elif validate_players_deck_choice(user_input):
                    break  # If input gets accepted, we break out of loop.

            # Split the users input into seperate variables.
            user_input = user_input.split('-')
            choice_1, choice_2 = None, None

            # Loop through all the piles, and assign the variables to the pile it's supposed to represent.
            for pile in self.card_piles:
                if user_input[0].upper() == pile.ID:
                    choice_1 = pile

                if user_input[1].upper() == pile.ID:
                    choice_2 = pile

            # Attempt to discard the top cards from the piles that the user has chosen.
            # If the piles are empty the program wil throw an error that needs to be handled.
            try:
                self.discard_pair(choice_1, choice_2)
                self.number_of_turns += 1
            except:
                return False

        # If the game is over. We return to the menu.
        else:
            initialize_menu()


def get_sub_deck(deck, start: int = 0, end: int = 1, ID: str = 'X'):
    '''This helper function takes in a deck of cards, and returns a new deck object that is a specified subset of the deck given as an argument.'''
    return Deck(deck.cards[start:end], ID)


def print_menu(language=Language):
    # if Language = 'NO':
    # Helper function that prints the menu in Norwegian.
    print(f'{"-"*50}')
    print(f'{" "*13} Wish Solitaire')
    print(f'{"-"*50}')
    print(f'{" "*13} 1. Start nytt spill')
    print(f'{" "*13} 2. Last inn spill')
    print(f'{" "*13} 3. Regler')
    print(f'{" "*13} 4. Avslutt spill')
    print(f'{"-"*50}')


def print_rules():
    # Helper function that prints the rules in Norwegian.
    print(f'{"-"*55}')
    print(f'{" "*22} Regler')
    print(f'{"-"*55}')
    print('Wish Solitaire bruker en 32 korts kortstokk hvor 2ere,')
    print('3ere, 4ere, 5ere og 6ere er fjernet fra bunken.')
    print('I starten av spillet blir korstokken tilfeldig')
    print('stokket om på, og fordelt på 8 bunker hvor toppkortet')
    print('alltid er synlig. Spilleren vinner når alle disse bunkene')
    print('er tømt. For å få lov til å fjerne kort, må spilleren')
    print('velge 2 bunker som har like topp-kort. Har kortene samme')
    print('verdi, kan begge fjernes for å så eksponere de nye topp')
    print('kortene. Finnes det ingen par blandt de 8 bunkene taper du.')
    print(f'{"-"*55}')


def initialize_menu():
    # Helper function that initiates the menu sequence.
    print_menu()
    wish_solitaire = Game()
    user_input = input(f" {' '*10} Tast in menyvalget >")
    if user_input == "1":
        # This while loop is essentially the entire game.
        while wish_solitaire.game_over == False:
            wish_solitaire.start_new_round()

    if user_input == "2":
        wish_solitaire.load_game()
        # This while loop is essentially the entire game.
        while wish_solitaire.game_over == False:
            wish_solitaire.start_new_round()

    if user_input == "3":
        print_rules()
        # The input below is only used in order to prevent the menu from popping up immediatly:
        proceed = input(f'{" "*10} Tast enter for å fortsette til menyen >')
        initialize_menu()
    if user_input == "4":
        print("Takk for idag!")
        quit()


initialize_menu()
